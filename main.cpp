#include <iostream>
#include <windows.h>
#include <cstdio>
#include "game/Maze.h"
#include "game/Mouse.h"

using namespace std;

#define DELAY_PER_TICK 25

int main()
{
    // Save the starting title and set it to a new one
    TCHAR oldTitle[MAX_PATH];
    GetConsoleTitle(oldTitle, MAX_PATH);
    SetConsoleTitle("Maze Project");

    // Save old screen buffer and set up a new one
    HANDLE oldBuffer = GetStdHandle(STD_OUTPUT_HANDLE);
    HANDLE currentBuffer = CreateConsoleScreenBuffer(GENERIC_READ | GENERIC_WRITE,
                                                     FILE_SHARE_READ | FILE_SHARE_WRITE,
                                                     NULL,
                                                     CONSOLE_TEXTMODE_BUFFER,
                                                     NULL);
    SetConsoleActiveScreenBuffer(currentBuffer);

    // Hide cursor
    CONSOLE_CURSOR_INFO cursor;
    cursor.bVisible = false;
    cursor.dwSize = 1;
    SetConsoleCursorInfo(currentBuffer, &cursor);

    // Set up the maze and the mouse
    Maze maze(&currentBuffer);
    Mouse mouse(&maze);

    // Keep moving until it's done
    while(!mouse.movementTick())
    {
        Sleep(DELAY_PER_TICK);
    }

    getchar();

    // Restore the starting title and buffer
    SetConsoleActiveScreenBuffer(oldBuffer);
    SetConsoleTitle(oldTitle);
    return 0;
}