
#ifndef MAZE_STACKNODE_H
#define MAZE_STACKNODE_H

template <typename T>
class StackNode
{
public:
    T data;
    StackNode* next;
    StackNode* prev;

    StackNode(T data)
    {
        this->data = data;
        this->next = nullptr;
        this->prev = nullptr;
    }
};


#endif //MAZE_STACKNODE_H
