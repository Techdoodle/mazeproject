
#ifndef MAZE_STACK_H
#define MAZE_STACK_H

#include "StackNode.h"

template <typename T>
class Stack
{
private:
    StackNode<T>* start;
    StackNode<T>* end;
    int length;

public:
    Stack()
    {
        start = nullptr;
        end = nullptr;
        length = 0;
    }

    void push(T data)
    {
        if(start == nullptr)
        {
            // Start the stack with the first node
            start = end = new StackNode<T>(data);
        }
        else
        {
            // Add to the stack and set up next/prev pointers
            end->next = new StackNode<T>(data);
            end->next->prev = end;
            end = end->next;
        }
        length++;
    }

    T pop()
    {
        if(start == nullptr)
        {
            // Should throw some error
            return start->data;
        }
        else
        {
            T data = end->data;
            if(end->prev != nullptr)
            {
                end = end->prev;
                delete end->next;
                end->next = nullptr;
            }
            else
            {
                // If this is the only element in the stack, reset the stack
                delete end;
                end = start = nullptr;
            }

            length--;

            return data;
        }
    }

    int getLength()
    {
        return length;
    }
};

#endif //MAZE_STACK_H
