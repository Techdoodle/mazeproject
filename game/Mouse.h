
#ifndef MAZE_MOUSE_H
#define MAZE_MOUSE_H


#include "Maze.h"
#include "../data/Stack.h"
#include "MouseCoord.h"

#define SPACE_GREEN 32
#define SPACE_YELLOW 224
#define SPACE_RED 192

class Mouse
{
private:
    int x, y;
    Maze* maze;
    bool visited[MAZE_HEIGHT][MAZE_WIDTH];
    Stack<MouseCoord> trail;

    bool hasVisited(int x, int y);
    void moveMouseTo(int x, int y, bool track, WORD color);
    void goBack();
public:
    Mouse(Maze* maze);
    bool movementTick();
};


#endif //MAZE_MOUSE_H
