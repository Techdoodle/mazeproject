
#ifndef MAZE_LANDMINE_H
#define MAZE_LANDMINE_H

struct Landmine
{
    int x, y;
    bool triggered;
};

#endif //MAZE_LANDMINE_H
