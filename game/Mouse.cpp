
#include "Mouse.h"

Mouse::Mouse(Maze *maze)
{
    this->maze = maze;
    this->x = 1;
    this->y = 0;
    for(int y = 0; y < MAZE_HEIGHT; y++)
    {
        for(int x = 0; x < MAZE_WIDTH; x++)
        {
            visited[y][x] = false;
        }
    }
}

bool Mouse::hasVisited(int x, int y)
{
    return (maze->isValid(x, y) && visited[y][x]);
}

void Mouse::moveMouseTo(int newX, int newY, bool track, WORD color)
{
    // If tracking is on, record this location in the stack
    if(track)
    {
        MouseCoord pos;
        pos.x = x;
        pos.y = y;
        trail.push(pos);
    }

    maze->drawMouse(x, y, newX, newY, color);
    x = newX;
    y = newY;
    visited[y][x] = true;
}

void Mouse::goBack()
{
    // Go to the previous location in the stack (while not recording it in the stack again)
    //visited[y][x] = true;
    MouseCoord pos = trail.pop();
    moveMouseTo(pos.x, pos.y, false, SPACE_RED);
}

bool Mouse::movementTick()
{
    // Check all four directions

    // Down
    if(!hasVisited(x, y + 1) && !maze->wallAt(x, y + 1))
    {
        moveMouseTo(x, y + 1, true, SPACE_YELLOW);
    }
    // Up
    else if(!hasVisited(x, y - 1) && !maze->wallAt(x, y - 1))
    {
        moveMouseTo(x, y - 1, true, SPACE_YELLOW);
    }
    // Right
    else if(!hasVisited(x + 1, y) && !maze->wallAt(x + 1, y))
    {
        moveMouseTo(x + 1, y, true, SPACE_YELLOW);
    }
    // Left
    else if(!hasVisited(x - 1, y) && !maze->wallAt(x - 1, y))
    {
        moveMouseTo(x - 1, y, true, SPACE_YELLOW);
    }
    // Trapped
    else
    {
        goBack();
    }

    if(maze->exitAt(x, y))
    {
        maze->drawPathGreen(trail);
        return true;
    }
    else if(maze->landmineAt(x, y))
    {
        // Reset the mouse back at the start
        while(trail.getLength() > 0)
        {
            trail.pop();
        }
        x = 1;
        y = 0;
        for(int y = 0; y < MAZE_HEIGHT; y++)
        {
            for(int x = 0; x < MAZE_WIDTH; x++)
            {
                visited[y][x] = false;
            }
        }
    }
    return false;
}