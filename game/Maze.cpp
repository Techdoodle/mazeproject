
#include <windows.h>
#include <wincon.h>
#include "Maze.h"

Maze::Maze(HANDLE *buffer)
{
    this->buffer = buffer;

    // Get the buffer size for offsets
    CONSOLE_SCREEN_BUFFER_INFO buffInfo;
    GetConsoleScreenBufferInfo(buffer, &buffInfo);

    SMALL_RECT windowSize = {0, 0, (MAZE_WIDTH - 1), (MAZE_HEIGHT - 1)};

    // Set the window size, the buffer size, and the window size again (needed to hide empty buffer scroll bar)
    SetConsoleWindowInfo(*buffer, TRUE, &windowSize);
    SetConsoleScreenBufferSize(*buffer, {MAZE_WIDTH, MAZE_HEIGHT});
    SetConsoleWindowInfo(*buffer, TRUE, &windowSize);

    // Read the file
    char ch;
    fstream mazeFile(MAZE_FILE, fstream::in);
    int x, y;
    x = y = landmineCount = 0;

    while(mazeFile >> ch)
    {
        data[y][x] = ch;

        if(ch == '2')
        {
            landmineCount++;
        }

        x++;
        if(x >= MAZE_WIDTH)
        {
            y++;
            x = 0;
        }
    }

    // Set up landmines
    landmines = new Landmine[landmineCount];
    int i = 0;
    for(y = 0; y < MAZE_HEIGHT; y++)
    {
        for(x = 0; x < MAZE_WIDTH; x++)
        {
            if(data[y][x] == '2')
            {
                landmines[i].x = x;
                landmines[i].y = y;
                landmines[i].triggered = false;
                i++;
            }
        }
    }

    // Draw the maze
    drawMaze();
}

Maze::~Maze()
{
    delete landmines;
}

void Maze::drawMaze()
{
    int i = 0;
    int landmine = 0;
    CHAR_INFO mazeText[MAZE_WIDTH * MAZE_HEIGHT];

    // Populate a CHAR_INFO array
    for(int y = 0; y < MAZE_HEIGHT; y++)
    {
        for(int x = 0; x < MAZE_WIDTH; x++)
        {
            mazeText[i].Attributes = FOREGROUND_BLUE | FOREGROUND_GREEN | FOREGROUND_RED;
            switch(data[y][x])
            {
                case '0':
                {
                    mazeText[i].Char.AsciiChar = ' ';
                    break;
                }
                case '1':
                {
                    mazeText[i].Char.AsciiChar = ((char) 219);
                    break;
                }
                case '2':
                {
                    if(landmines[landmine].triggered)
                    {
                        mazeText[i].Char.AsciiChar = ' ';
                    }
                    else
                    {
                        mazeText[i].Char.AsciiChar = 'O';
                    }
                    landmine++;
                    break;
                }
                case '3':
                {
                    mazeText[i].Char.AsciiChar = ')';
                    break;
                }
                default:
                {
                    mazeText[i].Char.AsciiChar = '?';
                    break;
                }
            }

            i++;
        }
    }

    // Print the text and draw the mouse at the starting pos
    writeOutput(0, 0, mazeText, MAZE_WIDTH, MAZE_HEIGHT);
    drawMouse(-1, -1, 1, 0, 0);
}

void Maze::writeOutput(short x, short y, CHAR_INFO *text, short columns, short rows)
{
    short finishX = x + columns;
    short finishY = y + rows;
    SMALL_RECT box = {x, y, finishX, finishY};

    WriteConsoleOutput(*this->buffer, text, {columns, rows}, {0, 0}, &box);
}

bool Maze::isValid(int x, int y)
{
    // Checks if the coordinates are within the bounds of the maze
    if(x < 0 || x >= MAZE_WIDTH)
    {
        return false;
    }
    if(y < 0 || y >= MAZE_HEIGHT)
    {
        return false;
    }
    return true;
}

bool Maze::wallAt(int x, int y)
{
    return (!isValid(x, y) || data[y][x] == '1');
}

bool Maze::exitAt(int x, int y)
{
    return (isValid(x, y) && data[y][x] == '3');
}

bool Maze::landmineAt(int x, int y)
{
    if(isValid(x, y) && data[y][x] == '2')
    {
        for(int i = 0; i < landmineCount; i++)
        {
            if(landmines[i].x == x && landmines[i].y == y)
            {
                if(landmines[i].triggered)
                {
                    // If the landmine has already been triggered, ignore it
                    return false;
                }
                else
                {
                    // If the landmine is active, explode
                    landmines[i].triggered = true;
                    shakeWindow();
                    drawMaze();
                    return true;
                }
            }
        }
    }
    return false;
}

void Maze::drawMouse(int oldX, int oldY, int x, int y, WORD color)
{
    // Remove the old position (if it exists)
    if(oldX != -1 && oldY != -1)
    {
        CHAR_INFO blank;
        blank.Char.AsciiChar = ' ';
        blank.Attributes = color;
        writeOutput(oldX, oldY, &blank, 1, 1);
    }

    // Draw the mouse at the new position
    CHAR_INFO mouse;
    mouse.Char.AsciiChar = '*';
    mouse.Attributes = FOREGROUND_BLUE | FOREGROUND_GREEN | FOREGROUND_RED;
    writeOutput(x, y, &mouse, 1, 1);
}

void Maze::drawPathGreen(Stack<MouseCoord> trail)
{
    // Draws the mouse trail in green
    WORD color[1];
    color[0] = 32;
    DWORD written;
    while(trail.getLength() > 0)
    {
        MouseCoord pos = trail.pop();
        WriteConsoleOutputAttribute(*this->buffer, color, 1, {static_cast<SHORT>(pos.x), static_cast<SHORT>(pos.y)}, &written);
    }
}

void Maze::shakeWindow()
{
    // Get the window handle and position
    HWND win = GetConsoleWindow();
    RECT consolePos;
    GetWindowRect(win, &consolePos);

    int width = consolePos.right - consolePos.left;
    int height = consolePos.bottom - consolePos.top;

    // Change the window position randomly to simulate explosion shaking
    for(int i = 0; i < 6; i++)
    {
        int changeX = (rand() % static_cast<int>(100 + 1)) - 50;
        int changeY = (rand() % static_cast<int>(100 + 1)) - 50;
        MoveWindow(win, consolePos.left + changeX, consolePos.top + changeY, width, height, true);
        Sleep(50);
    }
    for(int i = 0; i < 6; i++)
    {
        int changeX = (rand() % static_cast<int>(140 + 1)) - 70;
        int changeY = (rand() % static_cast<int>(140 + 1)) - 70;
        MoveWindow(win, consolePos.left + changeX, consolePos.top + changeY, width, height, true);
        Sleep(50);
    }
    for(int i = 0; i < 12; i++)
    {
        int changeX = (rand() % static_cast<int>(20 + 1)) - 10;
        int changeY = (rand() % static_cast<int>(20 + 1)) - 10;
        MoveWindow(win, consolePos.left + changeX, consolePos.top + changeY, width, height, true);
        Sleep(50);
    }

    // Reset back to it's starting position
    MoveWindow(win, consolePos.left, consolePos.top, width, height, true);
}