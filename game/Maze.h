
#ifndef MAZE_MAZE_H
#define MAZE_MAZE_H

#include <fstream>
#include <iostream>
#include <windows.h>
#include "MouseCoord.h"
#include "../data/Stack.h"
#include "Landmine.h"

using namespace std;

#define MAZE_FILE "maze.txt"
#define MAZE_HEIGHT 30
#define MAZE_WIDTH 100

class Maze
{
private:
    char data[MAZE_HEIGHT][MAZE_WIDTH];
    int landmineCount;
    Landmine* landmines;
    void drawMaze();
    void shakeWindow();
    void writeOutput(short x, short y, CHAR_INFO* text, short columns, short rows);

    HANDLE* buffer;
public:
    Maze(HANDLE* buffer);
    ~Maze();

    bool isValid(int x, int y);
    bool wallAt(int x, int y);
    bool exitAt(int x, int y);
    bool landmineAt(int x, int y);

    void drawMouse(int oldX, int oldY, int x, int y, WORD color);
    void drawPathGreen(Stack<MouseCoord> trail);
};


#endif //MAZE_MAZE_H
